﻿using BO;

namespace DAL;

public interface IHotelDal
{
    public Hotel ForGettingHotel();
    public void ForSettingHotelRooms(IEnumerable<ChambreHotel> chambreHotels);
}