﻿using BO;

namespace DAL;

public class HotelDal : IHotelDal
{
    private Hotel _hotelDefault = new Hotel()
    {
        chambres = new List<ChambreHotel>()
        {
            new ChambreHotel() { Etage = 0, Numero = 1, Prix = 50 },
            new ChambreHotel() { Etage = 0, Numero = 2, Prix = 50 },
            new ChambreHotel() { Etage = 1, Numero = 101, Prix = 53.5 },
            new ChambreHotel() { Etage = 1, Numero = 102, Prix = 53.5 },
            new ChambreHotel() { Etage = 1, Numero = 103, Prix = 53.5 },
            new ChambreHotel() { Etage = 2, Numero = 201, Prix = 61 },
            new ChambreHotel() { Etage = 2, Numero = 202, Prix = 61 },
            new ChambreHotel() { Etage = 3, Numero = 301, Prix = 66.5 },
        }
    };
    
    public Hotel ForGettingHotel()
    {
        return new Hotel()
        {
            chambres = new List<ChambreHotel>(_hotelDefault.chambres)
        };
    }

    public void ForSettingHotelRooms(IEnumerable<ChambreHotel> chambres)
    {
        _hotelDefault = new Hotel()
        {
            chambres = chambres
        };
    }
}