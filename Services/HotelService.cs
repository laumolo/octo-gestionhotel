﻿using BO;
using DAL;

namespace Services;

public class HotelService : IHotelService
{
    private readonly IHotelDal _hotelDal;

    public HotelService(IHotelDal hotelDal)
    {
        _hotelDal = hotelDal;
    }

    public IEnumerable<ChambreHotel> ForGettingHotelRooms()
    {
        return _hotelDal.ForGettingHotel().chambres;
    }

    public void ForEditingPriceOnFloorZero(double nouveauPrix)
    {
        var currentHotel = _hotelDal.ForGettingHotel();
        var chambres = new List<ChambreHotel>();
        foreach (var chambreHotel in currentHotel.chambres)
        {
            chambres.Add(new ChambreHotel()
            {
                Etage = chambreHotel.Etage,
                Numero = chambreHotel.Numero,
                Prix = DeterminerNouveauPrix(nouveauPrix, chambreHotel)
            });
        }

        _hotelDal.ForSettingHotelRooms(chambres);
    }

    private static double DeterminerNouveauPrix(double nouveauPrix, ChambreHotel chambreHotel)
    {
        double prix = 0;
        switch (chambreHotel.Etage)
        {
            case 0:
                prix = nouveauPrix < 200 ? nouveauPrix : 200;
                break;
            case 1:
                prix = (nouveauPrix * 1.07) < 200 ? nouveauPrix * 1.07 : 200;
                break;
            case 2:
                prix = (nouveauPrix * 1.22) < 200 ? nouveauPrix * 1.22 : 200;
                break;
            case 3:
                prix = (nouveauPrix * 1.33) < 200 ? nouveauPrix * 1.33 : 200;
                break;
        }

        return prix;
    }
}