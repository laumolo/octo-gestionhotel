﻿using BO;

namespace Services;

public interface IHotelService
{
    public IEnumerable<ChambreHotel> ForGettingHotelRooms();
    public void ForEditingPriceOnFloorZero(double nouveauPrix);
}