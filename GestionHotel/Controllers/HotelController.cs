using Microsoft.AspNetCore.Mvc;
using BO;
using Services;

namespace GestionHotel.Controllers;

[ApiController]
[Route("[controller]")]
public class HotelController : ControllerBase
{
    private readonly ILogger<HotelController> _logger;
    private readonly IHotelService _hotelService;

    public HotelController(ILogger<HotelController> logger, IHotelService hotelService)
    {
        _logger = logger;
        _hotelService = hotelService;
    }

    [HttpGet(Name = "GetChambresHotel")]
    public IEnumerable<ChambreHotel> Get()
    {
        return _hotelService.GetChambresHotel();
    }
}