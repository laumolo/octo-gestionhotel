using BO;
using DAL;
using Services;

namespace TestProject1;

public class TestChambresHotel
{
    [Fact]
    public void TestExcelFileIsTheSame()
    {
        IHotelDal hotelDataFromExcel = new HotelDataFromExcel();
        IHotelService hotelServiceFromExcel = new HotelService(hotelDataFromExcel);

        IHotelDal hotelDal = new HotelDal();
        IHotelService hotelService = new HotelService(hotelDal);

        var chambresHotelFromExcel = hotelServiceFromExcel.ForGettingHotelRooms();
        var chambresHotel = hotelService.ForGettingHotelRooms();

        bool everythingIsFine = true;

        foreach (var c1 in chambresHotelFromExcel)
        {
            everythingIsFine &= chambresHotel.Any(x => x.IsSame(x, c1));
        }

        Assert.True(everythingIsFine);

        Assert.Equal(chambresHotelFromExcel.Count(), chambresHotel.Count());
    }

    [Fact]
    public void TestAdministrateurChangePrix()
    {
        IHotelDal hotelDal = new HotelDal();
        IHotelService hotelService = new HotelService(hotelDal);

        var chambresHotel = hotelService.ForGettingHotelRooms();

        hotelService.ForEditingPriceOnFloorZero(100);

        var chambresHotelPrixModifies = hotelService.ForGettingHotelRooms();

        Assert.NotEqual(chambresHotel.First(x => x.Etage == 0), chambresHotelPrixModifies.First(x => x.Etage == 0));
        Assert.Equal(100, chambresHotelPrixModifies.First(x => x.Etage == 0).Prix);
    }

    [Theory]
    [InlineData(100)]
    [InlineData(200)]
    [InlineData(0)]
    [InlineData(int.MaxValue)]
    [InlineData(int.MinValue)]
    public void TestNouveauPrixEnregistre(double nouveauPrix)
    {
        IHotelDal hotelDal = new HotelDal();
        IHotelService hotelService = new HotelService(hotelDal);

        hotelService.ForEditingPriceOnFloorZero(nouveauPrix);

        var chambresHotel = hotelService.ForGettingHotelRooms();

        Assert.Equal(nouveauPrix < 200 ? nouveauPrix : 200, chambresHotel.First(x => x.Etage == 0).Prix);
        Assert.Equal(nouveauPrix * 1.07 < 200 ? nouveauPrix * 1.07 : 200, chambresHotel.First(x => x.Etage == 1).Prix);
        Assert.Equal(nouveauPrix * 1.22 < 200 ? nouveauPrix * 1.22 : 200, chambresHotel.First(x => x.Etage == 2).Prix);
        Assert.Equal(nouveauPrix * 1.33 < 200 ? nouveauPrix * 1.33 : 200, chambresHotel.First(x => x.Etage == 3).Prix);
    }

    [Fact]
    public void TestPlafondPrix()
    {
        IHotelDal hotelDal = new HotelDal();
        IHotelService hotelService = new HotelService(hotelDal);

        hotelService.ForEditingPriceOnFloorZero(170);

        var chambresHotelPrixModifies = hotelService.ForGettingHotelRooms();

        Assert.Equal(200, chambresHotelPrixModifies.First(x => x.Etage == 3).Prix);
    }
}

public class HotelDataFromExcel : IHotelDal
{
    public Hotel ForGettingHotel()
    {
        return new Hotel()
        {
            chambres = new List<ChambreHotel>()
            {
                new ChambreHotel() { Etage = 0, Numero = 1, Prix = 50 },
                new ChambreHotel() { Etage = 0, Numero = 2, Prix = 50 },
                new ChambreHotel() { Etage = 1, Numero = 101, Prix = 53.5 },
                new ChambreHotel() { Etage = 1, Numero = 102, Prix = 53.5 },
                new ChambreHotel() { Etage = 1, Numero = 103, Prix = 53.5 },
                new ChambreHotel() { Etage = 2, Numero = 201, Prix = 61 },
                new ChambreHotel() { Etage = 2, Numero = 202, Prix = 61 },
                new ChambreHotel() { Etage = 3, Numero = 301, Prix = 66.5 },
            }
        };
    }

    public void ForSettingHotelRooms(IEnumerable<ChambreHotel> chambres)
    {
        throw new NotImplementedException();
    }
}