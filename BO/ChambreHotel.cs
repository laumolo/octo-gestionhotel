﻿namespace BO;

public class ChambreHotel
{
    public int Etage { get; set; }
    public int Numero { get; set; }
    public double Prix { get; set; }


    public bool IsSame(ChambreHotel c1, ChambreHotel c2)
    {
        return c1.Etage == c2.Etage && c1.Numero == c2.Numero && c1.Prix == c2.Prix;
    }
}